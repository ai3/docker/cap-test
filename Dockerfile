FROM debian:stable

RUN apt-get -q update && apt-get install libcap2-bin

ENTRYPOINT ["/sbin/capsh", "--print"]

